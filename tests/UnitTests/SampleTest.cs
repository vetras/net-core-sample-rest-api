using Xunit;

namespace UnitTests
{
    public class SampleTest
    {
        [Fact]
        public void OneIsOne()
        {
            Assert.True(1 == 1);
        }

        [Fact]
        public void TwoIsTwo()
        {
            Assert.True(2 == 2);
        }
    }
}
