﻿using Microsoft.AspNetCore.Builder;
using Nancy;
using Nancy.Configuration;

namespace SampleRestApi
{
    public sealed class NancyBootstrapper : DefaultNancyBootstrapper
    {
        private readonly IApplicationBuilder app;

        public NancyBootstrapper(IApplicationBuilder app)
        {
            this.app = app;
        }

        public override void Configure(INancyEnvironment env)
        {
            env.Tracing(enabled: true, displayErrorTraces: true);
        }
        
    }
}
