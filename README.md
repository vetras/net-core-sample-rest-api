# Terminal commands

    dotnet clean 
    dotnet restore
    dotnet build
    

To run the app, execute the following command.
Then use postman to acces the rest api.

    dotnet run --project src\SampleRestApi\SampleRestApi.csproj

To create a deployable folder (IIS):

    dotnet publish --output ./app/ --configuration Release

# Docker

Move to the project root:

    cd ./src/SampleRestApi/

To build and run the docker image:

    docker build -t sampleapi .
    docker run -it -p 50421:80 sampleapi

    # -t: tag the image with a name (otherwise we need to use the image ID hash)
    # -i: interactive, the console stays open
    # -t: create a virtual terminal (shows logs of the container on the current console)
    # -p: TCP port mapping like host:container
