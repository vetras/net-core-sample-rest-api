﻿using Nancy;

namespace SampleRestApi.Modules
{
    public sealed class DojoModule : NancyModule
    {
        public DojoModule()
        {
            Get("/dojo", _ => "Hello ninja. Welcome to training.");
        }
    }
}
